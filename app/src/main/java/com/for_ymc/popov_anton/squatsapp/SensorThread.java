package com.for_ymc.popov_anton.squatsapp;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.FastDtwTest;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by vegastar on 19.10.2016.
 */

public class SensorThread extends Thread implements SensorEventListener {
    private Context context;
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private long lastUpdate;
    private long startTime;
    private final float RC = 150.0f;
    float accel_y_smoothed = 0.0f;
    private Handler handler;

    SensorManager sm;


    boolean listen_pattern =false;
    boolean listen_sources =false;
    String current_pattern_x;
    String current_pattern_y;
    String current_pattern_z;
    int listening_dots =0;
    int listening_dots_x =0;
    int listening_dots_y =0;
    int listening_dots_z =0;
    int counter = 0;
    String []some=new String[3];
    ArrayList<Float> data_x = new ArrayList<Float>();
    ArrayList<Float> data_y = new ArrayList<Float>();
    ArrayList<Float> data_z = new ArrayList<Float>();

    String path;
    int cont;


    public SensorThread(Context context, Handler handler) {
        this.context = context;
        this.handler = handler;
    }

    public void set_btn()
    {
        data_x.clear();
        data_y.clear();
        data_z.clear();
        listen_pattern = true;
    }
    public void stop_btn()
    {
        listen_pattern = false;
        listening_dots = 0;
        try
        {
            DateFormat df = new SimpleDateFormat("ddMMHHmmss");
            String date = df.format(Calendar.getInstance().getTime());
            current_pattern_x = "/x_pattern-"+date+".csv";
            current_pattern_y = "/y_pattern-"+date+".csv";
            current_pattern_z = "/z_pattern-"+date+".csv";
            FileWriter csvWrote_x = new FileWriter(path+current_pattern_x);
            FileWriter csvWrote_y = new FileWriter(path+current_pattern_y);
            FileWriter csvWrote_z = new FileWriter(path+current_pattern_z);
            for(Float p : data_x)
            {
                csvWrote_x.write(p.toString());
                csvWrote_x.write("\r\n");
                listening_dots_x++;
            }
            csvWrote_x.close();

            for(Float p : data_y)
            {
                csvWrote_y.write(p.toString());
                csvWrote_y.write("\r\n");
                listening_dots_y++;
            }
            csvWrote_y.close();

            for(Float p : data_z)
            {
                csvWrote_z.write(p.toString());
                csvWrote_z.write("\r\n");
                listening_dots_z++;
            }
            csvWrote_z.close();

            listening_dots= (listening_dots_z+listening_dots_y+listening_dots_x)/3;


        }
        catch (Exception E)
        {

        }
    }

    public void listen_btn()
    {
        listen_sources=true;
        counter = 0;
    }
    public void stop_listen_btn()
    {
        listen_sources = false;
    }

    public void run() {

        path= this.context.getFilesDir().getParentFile().getPath();
        Log.i("fff", "путь: "+ path);
        sm = (SensorManager) context.getSystemService(context.SENSOR_SERVICE);
        List<Sensor> sensors = sm.getSensorList(Sensor.TYPE_ACCELEROMETER);
        if (sensors.size() > 0) {
            boolean b = sm.registerListener(this, sensors.get(0), SensorManager.SENSOR_DELAY_GAME);
        }

        ///sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        //accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        //startTime = System.currentTimeMillis();
        //lastUpdate = System.currentTimeMillis();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // Gather Sensor data
        if (!Thread.interrupted()) {


            synchronized (this) {

                if(listen_pattern)
                {


                    data_x.add(event.values[0]);
                    data_y.add(event.values[1]);
                    data_z.add(event.values[2]);



                }
                else {

                    if (listen_sources) {
                        //Log.i("fff", "слушаем данные");
                        //curX = event.values[0];
                        //curY = event.values[1];
                        //curZ = event.values[2];
                        ArrayList<Float> coords = new ArrayList<Float>(3);
                        coords.add((float) (Math.round(event.values[0] * 1000.0) / 1000.0));
                        coords.add((float) (Math.round(event.values[1] * 1000.0) / 1000.0));
                        coords.add((float) (Math.round(event.values[2] * 1000.0) / 1000.0));

                        data_x.add(event.values[0]);
                        data_y.add(event.values[1]);
                        data_z.add(event.values[2]);

                        //ArrayList<Float> magn = new ArrayList<Float>(1);
                       // m = Math.sqrt(Math.pow(coords.get(0), 2) + Math.pow(coords.get(1), 2) + Math.pow(coords.get(2), 2));
                        //magn.add((float) (Math.round(m * 1000.0) / 1000.0));
                        //magnitud.add(magn);

                        cont++;
                        //Log.i("fff", "счетчик: "+ cont);
                        if (cont == listening_dots) {
                            Log.i("fff", "количество точек: " + listening_dots);
                            try {
                                write_data();
                            } catch (IOException e) {
                                e.printStackTrace();
                                Log.i("fff", "ошибка в write_data(): " + e);
                            }
                            double[] result = comparison();
                            //magnitud = new ArrayList<ArrayList<Float>>();
                            //------added-------
                            data_x.clear();
                            data_y.clear();
                            data_z.clear();
                            //------------------
                            Log.i("fff", "результат распознавания в слушателе: x - " + result[0]);
                            Log.i("fff", "результат распознавания в слушателе: y - " + result[1]);
                            Log.i("fff", "результат распознавания в слушателе: z - " + result[2]);
                            double recognitions = (result[0]+result[1]+result[2])/3;
                            if(recognitions<=200)
                            {
                                counter++;
                                //text.setText(String.valueOf(counter));
                                Message message = handler.obtainMessage();
                                Bundle bundle = new Bundle();
                                bundle.putString("cnt", String.valueOf(counter));
                                message.setData(bundle);
                                handler.sendMessage(message);
                            }
                            cont = 0;
                        }
                    }
                }
            }


        } else {
            stopThread();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void stopThread() {
        sensorManager.unregisterListener(this);
    }

    public void write_data() throws IOException {
        Log.i("fff", "записали данные");

        FileWriter csvWrote_x = new FileWriter(path+"/gest_x.csv");
        FileWriter csvWrote_y = new FileWriter(path+"/gest_y.csv");
        FileWriter csvWrote_z = new FileWriter(path+"/gest_z.csv");

        for (Float tmp : data_x) {
            csvWrote_x.write(tmp.toString());
            csvWrote_x.write("\r\n");
        }
        csvWrote_x.close();
        for (Float tmp : data_y) {
            csvWrote_y.write(tmp.toString());
            csvWrote_y.write("\r\n");
        }
        csvWrote_y.close();
        for (Float tmp : data_z) {
            csvWrote_z.write(tmp.toString());
            csvWrote_z.write("\r\n");
        }
        csvWrote_z.close();

    }


    public double[] comparison(){

        some[0]= path+current_pattern_x;
        some[1]= path + "/gest_x.csv";
        some[2]= "3";
        double result_x = 0;
        try{
            result_x = FastDtwTest.recognition(some);
        }
        catch (NullPointerException e){
            e.printStackTrace();
            Log.i("fff", "ошибка в comparison: "+ e);
        }

        some[0]= path+current_pattern_y;
        some[1]= path + "/gest_y.csv";
        some[2]= "3";
        double result_y = 0;
        try{
            result_y = FastDtwTest.recognition(some);
        }
        catch (NullPointerException e){
            e.printStackTrace();
            Log.i("fff", "ошибка в comparison y: "+ e);
        }

        some[0]= path+current_pattern_z;
        some[1]= path + "/gest_z.csv";
        some[2]= "3";
        double result_z = 0;
        try{
            result_z = FastDtwTest.recognition(some);
        }
        catch (NullPointerException e){
            e.printStackTrace();
            Log.i("fff", "ошибка в comparison z: "+ e);
        }

        return new double[]{result_x,result_y,result_z};


    }
}
