package com.for_ymc.popov_anton.squatsapp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;

import com.FastDtwTest;



public class Main extends Activity {




    Button set;
    Button stop;
    Button listen;
    Button stop_listen;
    TextView text;
    Handler handler;
    SensorThread st;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this.c = getApplicationContext();
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        text = (TextView)findViewById(R.id.gesto);

        set = (Button)findViewById(R.id.set);
        stop = (Button)findViewById(R.id.stop);
        listen = (Button)findViewById(R.id.listen);
        stop_listen = (Button)findViewById(R.id.stop_listen);
        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                Bundle bundle = msg.getData();
                String cnt = bundle.getString("cnt");
                text.setText(cnt);
            }
        };

        st = new SensorThread(this, handler);
        st.start();







    }





    @Override
    protected void onResume(){
        super.onResume();
        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    st.set_btn();
                }
                catch (Exception e)
                {

                }
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    st.stop_btn();
                }
                catch (Exception e)
                {

                }
            }
        });

        listen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    st.listen_btn();
                    text.setText("0");
                }
                catch (Exception e)
                {

                }
            }
        });

        stop_listen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    st.stop_listen_btn();
                }
                catch (Exception e)
                {

        }
            }
        });
    }




    @Override
    protected void onPause(){
        super.onPause();

    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        st.interrupt();
    }






}

